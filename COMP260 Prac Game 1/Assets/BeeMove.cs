﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// Use this for initialization


	
	// Update is called once per frame
	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target1;
	public Transform target2;
	private Transform target;
	public Vector2 heading = Vector3.right; 
	private Vector2 direction;
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;

    void Start()
    {
        PlayerMove player = FindObjectOfType<PlayerMove>();
        target1 = player.transform;
        PlayerTwoMove player2 = FindObjectOfType<PlayerTwoMove>();
        target2 = player2.transform;

        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,Random.value);
    }


	void Update() {
		// get the vector from the bee to the target 

		Vector2 direction1 = target1.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		// calculate how much to turn per frame
		if (direction1.magnitude < direction2.magnitude) {
			 direction = direction1;
		} else {
			 direction = direction2;
		}
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}
	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
    public ParticleSystem explosionPrefab;
    void OnDestroy()
    {
        
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }



}
