﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{

    public BeeMove beePrefab;
    public int nBee = 10;
    public float xMin, yMin;
    public float width, height;

    private float beePeriod;
    public float minBeePeriod = 1.0f, maxBeePeriod = 1.0f;
    // Use this for initialization
    void Start()
    {
        beePeriod = 0;
    }
    void Update()
    {
        float x, y;
        if (beePeriod <= 0)
        {
            BeeMove bee = Instantiate(beePrefab);
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee ";

            x = xMin + Random.value * width;
            y = yMin + Random.value * height;
            bee.transform.position = new Vector2(x, y);
            beePeriod = Mathf.Lerp(minBeePeriod, maxBeePeriod, Random.value);
        }
        beePeriod -= Time.deltaTime;
    }

    // Update is called once per frame
    public void DestroyBees(Vector2 centre, float radius)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }

        }
    }
}
