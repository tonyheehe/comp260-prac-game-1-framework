﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTwoMove : MonoBehaviour {
    private BeeSpawner beeSpawner;

    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }

    // Use this for initialization
    public float maxSpeed = 5.0f;
    public float destroyRadius = 1.0f;
    void Update () {
        if (Input.GetButtonDown("Fire2"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }

        Vector2 direction;
		direction.x = Input.GetAxis("Horizontal2");
		direction.y = Input.GetAxis("Vertical2");

		Vector2 velocity = direction * maxSpeed;

		transform.Translate(velocity * Time.deltaTime);
	}
}
