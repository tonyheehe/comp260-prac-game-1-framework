﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    private BeeSpawner beeSpawner;
        void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }


    public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 1.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 5.0f; // in metres/second/second
	public float turnSpeed = 30.0f;
    public float destoryRadius = 1.0f;

	void Update() {
        if (Input.GetButtonDown("Fire1"))
        {
            beeSpawner.DestroyBees(transform.position, destoryRadius);
        }
		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		float brakespeed = brake * Time.deltaTime;
		float turn = Input.GetAxis ("Horizontal");
		transform.Rotate (0, 0, turn *speed* turnSpeed * Time.deltaTime);
		
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} 
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} 
		else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
				if (speed < 0) {
					speed = 0;
				}
			} else {
				speed = speed + brake * Time.deltaTime;
				if (speed > 0) {
					speed = 0;
				}
			}
				
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime,Space.Self);

	}


}
